import requests
import pandas as pd
import numpy as np
import re
import requests_cache
from bs4 import BeautifulSoup
from jellyfish import levenshtein_distance


# set up a cache to minimize number of requests to the actual AWM server
# requests_cache.install_cache('awm_cache', backend='sqlite', expire_after=3600)


class Catalog:
    """
    Class to store recycling rules
    """

    def __init__(self):
        self.link = "https://www.awm-muenchen.de/index/abfalllexikon.html"
        self.catalog = self.get_catalog()
        self.known_categories = self.catalog["category"].str.lower().tolist()

    def get_catalog(self):
        """
        function to construct catalog from the results of the GET request
        :return:
        """
        r = requests.get(self.link)
        soup = BeautifulSoup(r.content, 'html.parser')

        # find all possible garbage types we can get
        # get separate list for each letter of the (german) alphabet
        # iterate over each of them to get garbage type, parent category and link to use.

        categories_catalog = []
        for lex_letter in soup.find_all("ul", class_="abfalllexikon"):
            links = lex_letter.find_all('a', href=True)
            for l in links:
                link = l["href"]
                text = l.get_text()

                if "(siehe" in text:
                    category = re.findall("([^(]+)", text)[0].strip()
                    parent_category = re.findall("\(siehe ([^)]+)\)", text)[0].strip()
                else:
                    category = text.strip()
                    parent_category = None

                # print(text, '->', category, '+', parent_category)
                categories_catalog.append({"category": category, "parent_category": parent_category, "link": link})

        return pd.DataFrame(categories_catalog)

    def correct_spelling(self, word, vocab=None, d=2):
        """
        Check given word against given dictionary to find the closest item of (with distance >= d)
        and return correction
        :param word: str - message from user
        :param vocab: list - list of options to match message with, if None - self.catalog
        :param d: - min distance to be a match
        :return: correction
        """

        if vocab is None:
            vocab = self.known_categories

        # get edit distance from word to each item of vocabulary
        distances = [levenshtein_distance(word, item) for item in vocab]
        # if all the words in the vocab are further than distance d, return None
        if np.min(distances) > d:
            return None
        else:
            # else return first vocab entry with the closest edit distance to the word
            index_min = np.argmin(distances)
            return vocab[index_min]

    def recognize_category(self, possible_category):
        """
        find closest category in the catalog
        :param possible_category: str
        :return: category that is recognized
        """
        if possible_category.lower() in self.known_categories:
            # this will return name of the category if it is in the list
            return possible_category
        else:
            # otherwise spell checker will return closes category from the
            # catalog if match is found or None, if there is no match
            return self.correct_spelling(possible_category.lower())

    def get_from_catalog(self, category, param):
        """
        Get link if such category exists
        :param category: category to get link for
        :param param: list of columns from self.catalog
        :return: link if category is found
        """

        if min([x in self.catalog.columns for x in param]) == 0:
            print("column list isn't recognized")
            return None
        else:
            cat = self.recognize_category(category.lower())
            if not cat:
                print("category isn't recognized")
                return None

            return_list = []
            for col in param:
                return_list.append(self.catalog.loc[self.catalog["category"].str.lower() == cat, col].tolist()[0])

            return return_list