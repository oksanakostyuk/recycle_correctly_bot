# import everything
import telebot
from telebot.types import InlineKeyboardButton, InlineKeyboardMarkup
from awm_requests import *
from botfiles.credentials import bot_token
import os

global bot
global TOKEN

TOKEN = bot_token
bot = telebot.TeleBot(token=TOKEN)

# set the port number to listen for the webhook
PORT = int(os.environ.get('PORT', 5000))

# initialize catalog with garbage categories.
Catalog = Catalog()

@bot.message_handler(commands=['start'])
def send_welcome(message):
    bot.send_message(message.from_user.id,
                     f"""Здарова бродяга! \n Здесь ты можешь """)

@bot.message_handler(commands=['all'])
def send_all_categories(message):
    # TODO: add inline category index
    bot.send_message(message.from_user.id, "\n".join(Catalog.known_categories)[0:3000])

@bot.message_handler(content_types=['text'])
def get_text_messages(message):
    print(message.text)
    if message.text.lower() == "здарова":
        bot.send_message(message.from_user.id, "и тебе не хворать")
    else:

        category = Catalog.recognize_category(message.text.lower())

        if not category:
            # если категория не найдена в списке совсем, то возвращается грустное сообщение
            bot.send_message(message.from_user.id, "блин, такого нет у меня в списке :(")

        if category:
            # notify user, if category is recognized but fixed by spell checker:
            if category != message.text.lower():
                bot.send_message(message.from_user.id, f"кажется, ты имеешь в виду {category}")

            print("category:", category)
            # query params for the message from Catalog
            parent_cat, link = Catalog.get_from_catalog(category, ['parent_category', 'link'])
            print(parent_cat, link)
            if parent_cat:
                message_txt = f"сортируй как {parent_cat}"
            else:
                message_txt = f"тут отдельная темка есть, почитай по ссылке"

            reply_markup = InlineKeyboardMarkup(row_width=1)
            reply_markup.add(InlineKeyboardButton(text='страница на сайте AWM', url=link))

            bot.send_message(message.from_user.id, message_txt, reply_markup=reply_markup)

bot.polling(none_stop=True)

bot.set_webhook()
updater.start_webhook(listen="0.0.0.0",
                          port=int(PORT),
                          url_path=TOKEN)
updater.bot.setWebhook('https://yourherokuappname.herokuapp.com/' + TOKEN)